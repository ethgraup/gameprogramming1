﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackkgroundScript : MonoBehaviour
{
    private GameObject manager;
    private ManagementScript ms;
    private Transform cameraTransform;
    private GameObject camera;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager");
        ms = manager.GetComponent<ManagementScript>();
        camera = ms.camera;
        cameraTransform = camera.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 temp = transform.position;

        temp.x = cameraTransform.position.x;
        temp.y = cameraTransform.position.y;

        transform.position = temp;
    }
}
