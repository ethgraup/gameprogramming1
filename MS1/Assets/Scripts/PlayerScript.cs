﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public Vector2 speed = new Vector2(50, 50);
    private Animator animator;
    private SpriteRenderer renderer;
    private Rigidbody2D rigidBody;
    public Vector2 jump = new Vector2(0, 10);
    void Start()
    {
        animator = GetComponent<Animator>();
        renderer = GetComponent<SpriteRenderer>();
        rigidBody = GetComponent<Rigidbody2D>();
    }
    // Update is called once per frame
    void Update(){
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        if(inputX > 0.1)
        {
            renderer.flipX = false;
        } else if(inputX < -0.1)
        {
            renderer.flipX = true;
        }
        if(Input.GetButtonDown("Jump") && rigidBody.velocity.y == 0)
        {
            rigidBody.AddForce(jump * speed.y, ForceMode2D.Impulse);
        }
        Vector3 movement = new Vector3(speed.x * inputX, 0, 0);

        movement *= Time.deltaTime;
        animator.SetFloat("HorizSpd", Mathf.Abs(speed.x * inputX));
        animator.SetFloat("VertSpd", rigidBody.velocity.y);
        transform.Translate(movement);
    }
}
