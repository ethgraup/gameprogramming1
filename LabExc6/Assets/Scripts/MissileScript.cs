﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileScript : MonoBehaviour
{
	public float speedX = 0.5f;
	public float speedY = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movement = new Vector2(speedX, speedY);
        movement *= Time.deltaTime;
        transform.Translate(movement);
    }
}
