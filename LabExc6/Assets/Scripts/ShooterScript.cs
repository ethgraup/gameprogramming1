﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterScript : MonoBehaviour
{
	public GameObject projectilePrefab;
	protected GameObject projectileShot;
	public int decayTime = 5;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump")){
        	projectileShot = Instantiate<GameObject>(projectilePrefab, transform.position, Quaternion.identity);
        	Destroy(projectileShot, decayTime);
        }
    }
}
