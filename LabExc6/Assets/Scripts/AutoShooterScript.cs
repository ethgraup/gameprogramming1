﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoShooterScript : MonoBehaviour
{
	public GameObject projectilePrefab;
	protected GameObject projectileShot;
	public float freq = 5.0f;
	public int decayTime = 4;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("shootProjectile", 1, freq);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void shootProjectile(){
        	projectileShot = Instantiate<GameObject>(projectilePrefab, transform.position, Quaternion.identity);
        	Destroy(projectileShot, decayTime);
    }
}
