﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManagementScript : MonoBehaviour
{
    public GameObject player;
    public GameObject background;
    public List<GameObject> enemies;
    public GameObject bulletCount;
    public GameObject ui;
    public float zombSpeed = 2.5f;
    public bool outOfBullets = false;
    public Vector3 exitLocation = new Vector3(0,0,0);
    
    public GameObject exitPrefab;
    public GameObject sceneChangePrefab;
    
    private bool exitSpawned = false;
    public bool levelOver = false;
    
    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        background = GameObject.FindGameObjectWithTag("Background");
        enemies = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
        ui = GameObject.FindGameObjectWithTag("UI");
    }

    // Update is called once per frame
    void Update()
    {
		for(int i = enemies.Count-1; i > -1; i--){
			if(enemies[i] == null){
				enemies.RemoveAt(i);
			}
		}
		if(enemies.Count == 0 && !exitSpawned){
			//Instantiate Exit
			Instantiate(exitPrefab, exitLocation, Quaternion.identity);
			Debug.Log("All Enemies Dead");
			exitSpawned = true;
		}
    }
    //Used to decay enemy corpses
    void PrepToDestroy(GameObject target){
    	Destroy(target, 5);
    }
    //Used when a bullet hits an enemy
    void DestroyNow(Object target){
    	Destroy(target);
    }
    //Used for blood spurts and quick particle systems
    void QuickDestroy(GameObject target){
    	Destroy(target, 0.5f);
    }
    void Gunshot(){
    	ui.GetComponentInChildren<Text>().SendMessage("GunFired");
    }
    void oob(){
    	outOfBullets = true;
    }
    void RefillAmmo(){
    	ui.GetComponentInChildren<Text>().SendMessage("AmmoRefill");
    	outOfBullets = false;
    }
    void RefillHealth(){
    	ui.GetComponentInChildren<Scrollbar>().SendMessage("HealDamage", 0.5);
    }	
    void FinishLevel(){
    	levelOver = true;
    	GameObject scp = Instantiate(sceneChangePrefab);
    	scp.SendMessage("GoodEnd");
    }
    void TakeDamage(float f){
    	ui.GetComponentInChildren<Scrollbar>().SendMessage("ReceiveDamage", f);
    }
    void KillPlayer(){
    	player.SendMessage("Die");
    	levelOver = true;
    	GameObject scp = Instantiate(sceneChangePrefab);
    	scp.SendMessage("BadEnd");
    }
    
}
