﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadeToBlackScript : MonoBehaviour
{
    // Start is called before the first frame update
    private int ending;
    public GameObject deathScreen;
    void BadEnd(){
    	ending = 1;
    }
    void GoodEnd(){
    	ending = 2;
    }
    public void CheckFinalResult(){
    	if(ending == 2){
    		//Change to scene
    		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    	}else if(ending == 1){
    		//Instantiate "Game Over" text prefab
    		Instantiate(deathScreen);
    	}else{
    		Debug.Log("Error, invalid ending");
    	}
    }
}
