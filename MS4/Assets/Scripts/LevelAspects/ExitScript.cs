﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitScript : MonoBehaviour
{
	public float fadeInTime = 3.0f;
	public GameObject manager;
    // Start is called before the first frame update
    void Start()
    {
    	manager = GameObject.FindGameObjectWithTag("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter2D(Collider2D col){
    	if(col.gameObject.tag == "Player"){
    		Debug.Log("Level Complete!");
    		manager.SendMessage("FinishLevel");
    		gameObject.GetComponent<AudioSource>().Play();
    	}
    }
}
