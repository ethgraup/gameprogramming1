﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : MonoBehaviour
{
	private GameObject gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void ReceiveDamage(float f){
    	gameObject.GetComponent<Scrollbar>().size -= f;
    	if(gameObject.GetComponent<Scrollbar>().size <= 0)
    		gameManager.SendMessage("KillPlayer");
    }
    void HealDamage(float f){
    	gameObject.GetComponent<Scrollbar>().size += f;
    	gameObject.GetComponent<AudioSource>().Play();
    	if(gameObject.GetComponent<Scrollbar>().size >= 1)
    		gameObject.GetComponent<Scrollbar>().size = 1;
    }
}
