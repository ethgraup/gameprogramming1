﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BulletCountScript : MonoBehaviour
{
	private Text t;
	private GameObject manager;
	public int bulletMax = 16;
	public int numBullets;
    // Start is called before the first frame update
    void Start()
    {
        t = gameObject.GetComponent<Text>();
        manager = GameObject.FindGameObjectWithTag("GameManager");
        numBullets = bulletMax;
    }

    // Update is called once per frame
    void Update()
    {
        t.text = "x" + numBullets;
    }
    void GunFired(){
    	if(numBullets > 0){
	    	numBullets--;
	    }
	    if(numBullets == 0){
	    	manager.SendMessage("oob");
	    }
    }
    void AmmoRefill(){
    	numBullets += bulletMax;
    	gameObject.GetComponent<AudioSource>().Play();
    }
}
