﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoPickupScript : MonoBehaviour
{
	GameObject manager;
	void Start(){
		manager = GameObject.FindGameObjectWithTag("GameManager");
	}
    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D col){
    	if(col.gameObject.tag == "Player"){
    		manager.SendMessage("RefillAmmo");
    		manager.SendMessage("DestroyNow", gameObject);
    	}
    }		
}
