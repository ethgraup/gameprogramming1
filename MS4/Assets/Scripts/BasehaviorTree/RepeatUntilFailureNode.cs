﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatUntilFailureNode : Decorator
{
    public RepeatUntilFailureNode(BehaviorTree t, BTNode c) : base(t, c){
    	}
    public override Result Execute(){
    	//Debug.Log("Child returned: " + Child.Execute());
    	if(Child.Execute() == Result.Failure){
    		return Result.Failure;
    	}else{
	    	return Result.Running;
	    }
    }
}
