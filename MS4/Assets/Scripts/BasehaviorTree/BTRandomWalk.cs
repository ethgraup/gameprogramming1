﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTRandomWalk : BTNode
{
	protected Vector3 NextDestination { get; set; }
	public float speed = 1.0f;
	public GameObject player;
	public EnemyScript es;
	
    public BTRandomWalk(BehaviorTree t) : base(t){
    	NextDestination = Tree.gameObject.transform.position;
    	FindNextDestination();
    	player = GameObject.FindGameObjectWithTag("Player");
    	es = Tree.gameObject.GetComponent<EnemyScript>();
    }
    
    public bool FindNextDestination(){
    	object o;
    	bool found = false;
    	found = Tree.Blackboard.TryGetValue("WorldBounds", out o);
    	if(found){
    		Rect bounds = (Rect)o;
    		float x = NextDestination.x + (UnityEngine.Random.value * bounds.width - (bounds.width/2));
    		NextDestination = new Vector3(x, NextDestination.y, NextDestination.z);
    	}
    	
    	return found;
    }
    public override Result Execute(){
    	//If close enough to the player, return failure and stop process
    	float diffX = Tree.gameObject.transform.position.x - player.transform.position.x;
    	diffX = diffX * diffX;
    	float diffY = Tree.gameObject.transform.position.y - player.transform.position.y;
    	diffY = diffY * diffY;
    	if(diffX + diffY < 49){
    		return Result.Failure;
    	}
    	//If we've arrived, find next destination
    	if(Mathf.Abs(Tree.gameObject.transform.position.x - NextDestination.x) < 0.1){
    		if(!FindNextDestination()){
    			return Result.Failure;
    		}else{
    			return Result.Success;
    		}
    	}else{
    		es.CheckWalk(NextDestination);
    		return Result.Running;
    	}
    }
}