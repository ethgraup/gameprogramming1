﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTTargettedWalk : BTNode
{
	public float speed = 1.0f;
	public GameObject player;
	public EnemyScript es;
	
	public BTTargettedWalk(BehaviorTree t) : base(t){
    	player = GameObject.FindGameObjectWithTag("Player");
    	es = Tree.gameObject.GetComponent<EnemyScript>();
    }
    public override Result Execute(){
    	es.MoveTowardsPlayer();
    	return Result.Running;
    }
}
