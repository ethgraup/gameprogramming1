﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EndScreenScript : MonoBehaviour
{
    // Start is called before the first frame update
    public void StartNewGame(){
    	SceneManager.LoadScene(0);
    }
    public void EndGame(){
		Application.Quit();
    }
    public void EnterCredits(){
    	SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
