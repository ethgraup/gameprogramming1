﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolScript : MonoBehaviour
{
	public float speed;
	public List<Vector3> waypoints;
	public List<Color> colors;
	
	private Coroutine path;
	private Coroutine colorShift;
	
    // Start is called before the first frame update
    void Start()
    {
        path = StartCoroutine(PatrolWaypoints());
        colorShift = StartCoroutine(InterpolateColors());
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Jump"))
        	gameObject.SendMessage("StopPatrolling");
    }
    
    public virtual void StopPatrolling(){
    	StopCoroutine(path);
    }
    
    public IEnumerator PatrolWaypoints(){
    	while(true){
    		foreach(Vector3 point in waypoints){
    			Debug.Log("Pathing to: " + point);
    			while(transform.position != point){
    				transform.position = Vector3.MoveTowards(transform.position, point, speed);
    				
    				yield return null;
    			}
    		}
    	}
    }
    public IEnumerator InterpolateColors(){
    	SpriteRenderer sr = GetComponent<SpriteRenderer>();
    	while(true){
    		foreach(Color c in colors){
    			Color d = sr.color;
    			float t = 0.0f;
    			Debug.Log("Shifting to: " + c);
    			while(sr.color != c){
    				t += 0.01f;
    				sr.color = Color.Lerp(d, c, t);
    				yield return null;
    			}
    		}
    	}
    }
}
