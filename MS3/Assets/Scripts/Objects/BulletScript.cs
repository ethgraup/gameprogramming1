﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
	private float speed = 12.0f;
	private int direction;
	private GameObject player;
	private GameObject manager;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        manager = GameObject.FindGameObjectWithTag("GameManager");
        if(player.GetComponent<SpriteRenderer>().flipX){
        	direction = -1;
        	GetComponent<SpriteRenderer>().flipX = false;
        }else{
        	direction = 1;
        	GetComponent<SpriteRenderer>().flipX = true;
        }
        manager.SendMessage("PrepToDestroy", gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movement = new Vector2(speed * direction, 0);
        movement *= Time.deltaTime;
        transform.Translate(movement);
    }
    void OnCollisionEnter2D(Collision2D col){
    	if(col.gameObject.tag == "Enemy"){
    		col.gameObject.SendMessage("Shot", 1);
    		col.gameObject.GetComponent<Rigidbody2D>().AddForce(new Vector2(2 * direction, 0), ForceMode2D.Impulse);
			manager.SendMessage("DestroyNow", gameObject);
    	}
    }
}
