﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 2.0f;
    public Vector2 direction;
    public int shotsNeeded = 3;
    private ManagementScript manager;
    private Animator animator;
    protected bool isDead = false;
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<ManagementScript>();
        animator = GetComponent<Animator>();
        speed = manager.zombSpeed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
    	if(!isDead && !manager.levelOver){
	        Vector2 displacement = new Vector2(manager.player.transform.position.x - gameObject.transform.position.x, 0);
	        direction = displacement/Mathf.Abs(manager.player.transform.position.x - gameObject.transform.position.x);
	        if(direction.x < 0){
	        	gameObject.GetComponent<SpriteRenderer>().flipX = true;
	        }else{
	        	gameObject.GetComponent<SpriteRenderer>().flipX = false;
	        }
	        Vector2 movement = direction * speed;
	    	transform.Translate(movement*Time.deltaTime);
	    	animator.SetFloat("HorizSpd", Mathf.Abs(movement.x));
	    }
    }
    void OnCollisionEnter2D(Collision2D col){
    	if(col.gameObject.tag == "Player" && !manager.levelOver){
    		animator.SetBool("Attack", true);
    		col.gameObject.SendMessage("HurtAndPush", 70 * direction.x);
    		gameObject.GetComponent<AudioSource>().Play();
    	}
    }
    void Shot(int a){
    	shotsNeeded -= a;
    	animator.SetBool("Shot", true);
    	if(shotsNeeded <= 0){
			isDead = true;
    		animator.SetBool("Dead", true);
    		Destroy(gameObject.GetComponent<Collider2D>());
    		Destroy(gameObject.GetComponent<Rigidbody2D>());
    		manager.SendMessage("PrepToDestroy", gameObject);
    	}
    }
    public void SwitchAttacked(GameObject nullobj){
    	animator.SetBool("Shot", false);
    }
    public void SwitchAttacking(GameObject nullobj){
    	animator.SetBool("Attack", false);
    }
}
