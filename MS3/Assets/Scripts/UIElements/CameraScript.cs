﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private GameObject manager;
    private ManagementScript ms;
    private Transform playerTransform;
    private GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager");
        ms = manager.GetComponent<ManagementScript>();
        player = ms.player;
        playerTransform = player.transform;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 temp = transform.position;

        temp.x = playerTransform.position.x;
        temp.y = playerTransform.position.y + 1;

        transform.position = temp;
    }
}
