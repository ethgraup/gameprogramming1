﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalMovingPlatScript : MonoBehaviour
{
	public Vector2 speed = new Vector2(0.0f, -1.0f);
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("SwitchDirection", 2, 2);
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 movement = speed * Time.deltaTime;
        transform.Translate(movement);
    }
    void SwitchDirection(){
    	speed.y *= -1;
    }
}
