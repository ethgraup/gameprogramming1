﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject gameManager;
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void sendString(){
    	string s = gameObject.GetComponent<InputField>().textComponent.text;
    	gameObject.GetComponent<InputField>().text = "";
    	gameManager.GetComponent<ManagerScript>().DebugLog(s);
    }
}
