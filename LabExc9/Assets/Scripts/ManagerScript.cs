﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject circle;
    public Color color;
    public string s;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void SpawnCircle(){
    	GameObject newCircle = Instantiate(circle);
    	newCircle.GetComponent<SpriteRenderer>().material.color = color;
    }
    void SetColorRed(){
    	color = Color.red;
    }
    void SetColorBlue(){
    	color = Color.blue;
    }
    void SetColorGreen(){
    	color = Color.green;
    }
    public void DebugLog(string s){
    	Debug.Log(s);
    }
}
