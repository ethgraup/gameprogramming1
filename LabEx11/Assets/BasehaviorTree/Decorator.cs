﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Decorator : BTNode
{
	public BTNode Child { get; set; }
	
    public Decorator(BehaviorTree t, BTNode c) : base(t){
    	Child = c;
    	
    }
    
}
