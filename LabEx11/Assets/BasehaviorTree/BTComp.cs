﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTComp : BTNode
{
	public List<BTNode> Children { get; set; }
	
    public BTComp(BehaviorTree t, BTNode[] nodes) : base(t){
    	Children = new List<BTNode>(nodes);
    	
    }
    
}