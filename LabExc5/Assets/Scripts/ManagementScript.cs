﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagementScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject[] planets;
    public GameObject camera;
    public int universeSize = 4;
    void Start()
    {
        planets = new GameObject[universeSize];
        planets = GameObject.FindGameObjectsWithTag("Planet");
        camera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
