﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
	public Vector2 speed = new Vector2(50,50);
	private GameObject manager;
	private ManagementScript ms;
    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.FindGameObjectWithTag("GameManager");
        ms = manager.GetComponent<ManagementScript>();
    }

    // Update is called once per frame
    void Update()
    {
        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        float left, right, top, bottom;
        Bounds size;
        Vector2 movement = new Vector2(speed.x * inputX, speed.y * inputY);
        movement *= Time.deltaTime;
        transform.Translate(movement);
        if(Input.GetMouseButtonDown(0)){
        	Debug.Log("Clicked!");
        	Vector3 mousePosition = Input.mousePosition;
        	mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
        	for(int i = 0; i < ms.universeSize; i++){
        		size = ms.planets[i].GetComponent<SpriteRenderer>().bounds;
        		mousePosition.z = ms.planets[i].transform.position.z;
        		if(size.Contains(mousePosition)){
        			Vector3 newPos = ms.planets[i].transform.position;
        			newPos.z = transform.position.z;
        			transform.position = newPos;
        		}
	        }
        }
    }
}
