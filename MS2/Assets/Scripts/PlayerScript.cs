﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    public Vector2 speed = new Vector2(50, 50);
    private Animator animator;
    private SpriteRenderer srenderer;
    private Rigidbody2D rigidBody;
    private ManagementScript manager;
    public Vector2 jump = new Vector2(0, 10);
    protected bool jumping = false;
    protected bool isHit = false;
    public GameObject prefab;
    void Start()
    {
        animator = GetComponent<Animator>();
        srenderer = GetComponent<SpriteRenderer>();
        rigidBody = GetComponent<Rigidbody2D>();
        manager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<ManagementScript>();
    }
    // Update is called once per frame
    void Update(){
    	if(!manager.levelOver){
	    	if(Input.GetButtonDown("Jump") && (rigidBody.velocity.y < 0.1 && rigidBody.velocity.y > -0.1))
	    	{
	    		jumping = true;
	    	}
	    	if(Input.GetButtonDown("Shoot")){
	    		animator.SetBool("Shooting", true);
	    		int direction = 1;
	    		if(srenderer.flipX){
	    			direction = -1;
	    		}
	    		Vector2 newPos = new Vector2(transform.position.x + (0.5f * direction), transform.position.y + 0.1f);
	    		Instantiate(prefab, newPos, Quaternion.identity);
	    	}
	    }
	}
    void FixedUpdate(){
   		if(!manager.levelOver){
	        float inputX = Input.GetAxis("Horizontal");
	        float inputY = Input.GetAxis("Vertical");
	        if(inputX > 0.1)
	        {
	            srenderer.flipX = false;
	        } else if(inputX < -0.1)
	        {
	            srenderer.flipX = true;
	        }
	        if(animator){
		        if(jumping)
	    	    {
	        	    rigidBody.AddForce(jump * speed.y, ForceMode2D.Impulse);
	        	    jumping = false;
	            	animator.SetBool("InAir", true);
	        	}
	        }
	        Vector3 movement = new Vector3(speed.x * inputX, 0, 0);
	
	        movement *= Time.deltaTime;
	        animator.SetFloat("HorizSpd", Mathf.Abs(speed.x * inputX));
	        animator.SetFloat("VertSpd", rigidBody.velocity.y);
	        transform.Translate(movement);
	    }else{
	    	animator.SetFloat("HorizSpd", 0);
	    	animator.SetFloat("VertSpd", 0);
	    }
    }
    void OnCollisionStay2D(Collision2D col){
    	if(col.gameObject.tag == "Ground" && rigidBody.velocity.y == 0){
    		animator.SetBool("InAir", false);
    	}
    }
    void OnCollisionExit2D(Collision2D col){
    	if(col.gameObject.tag == "Ground"){
    		animator.SetBool("InAir", true);
    	}
    }
    void HurtAndPush(float a){
		animator.SetBool("Hurt", true);
    	Debug.Log("Touched Enemy");
    	Vector2 push = new Vector2(a, 0);
    	rigidBody.AddForce(push, ForceMode2D.Impulse);
    }
    public void SwitchHurt(GameObject nullobj){
    	animator.SetBool("Hurt", false);
    }
    public void SwitchShoot(GameObject nullobj){
    	animator.SetBool("Shooting", false);
    }
}
