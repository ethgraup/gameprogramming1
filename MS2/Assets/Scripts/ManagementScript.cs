﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagementScript : MonoBehaviour
{
    public GameObject player;
    public GameObject background;
    public List<GameObject> enemies;
    public float zombSpeed = 2.5f;
    public GameObject exitPrefab;
    public GameObject sceneChangePrefab;
    private bool exitSpawned = false;
    public bool levelOver = false;
    // Start is called before the first frame update
    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        background = GameObject.FindGameObjectWithTag("Background");
        enemies = new List<GameObject>(GameObject.FindGameObjectsWithTag("Enemy"));
    }

    // Update is called once per frame
    void Update()
    {
		for(int i = enemies.Count-1; i > -1; i--){
			if(enemies[i] == null){
				enemies.RemoveAt(i);
			}
		}
		if(enemies.Count == 0 && !exitSpawned){
			//Instantiate Exit
			Instantiate(exitPrefab);
			Debug.Log("All Enemies Dead");
			exitSpawned = true;
		}
    }
    void PrepToDestroy(GameObject target){
    	Destroy(target, 5);
    }
    void DestroyNow(Object target){
    	Destroy(target);
    }
    void FinishLevel(){
    	levelOver = true;
    	Instantiate(sceneChangePrefab);
    }
}
